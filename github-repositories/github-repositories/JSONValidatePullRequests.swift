

import Foundation

struct JSONValidatePullRequests {

    func convertToPullRequests(response: Array<NSDictionary>) -> [PullRequest] {
    
        var pullRequests = [PullRequest]()
        
        for info in response {
            let pullRequest = PullRequest(json: info)
            pullRequests.append(pullRequest)
        }
        
        return pullRequests
    }
}

    
