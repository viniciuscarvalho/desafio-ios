

import UIKit

class PullRequest {
    
    var title: String?
    var body: String?
    var url: String?
    var date: String?
    var nameAuthor: String?
    var avatarURL: String?
    
    init(json: NSDictionary) {
        self.title = json["title"] as? String
        self.body = json["body"] as? String
        self.url = json["html_url"] as? String
        self.date = json["created_at"] as? String
        
        let user = json["user"] as! NSDictionary
        let login = user.valueForKey("login")
        self.nameAuthor = login as? String
        
        let avatar = user.valueForKey("avatar_url")
        self.avatarURL = avatar as? String
    }

}
