

import UIKit

class RepositoriesTableViewCell: UITableViewCell{
    
    @IBOutlet weak var repositoryNameLbl: UILabel!
    @IBOutlet weak var descriptionLbl: UILabel!
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var userNameLbl: UILabel!
    @IBOutlet weak var forksLbl: UILabel!
    @IBOutlet weak var starsLbl: UILabel!
    
    var repository: Repository!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.userImage.layer.cornerRadius = CGRectGetWidth(self.userImage.frame) / 2
        self.userImage.clipsToBounds = true;
        self.userNameLbl.adjustsFontSizeToFitWidth = true;
        self.userNameLbl.minimumScaleFactor = 0.5
        
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
}
