

import Foundation

struct JSONValidateRepositories {
    
    func convertToRepository(response: [NSDictionary]) -> [Repository] {
        
        let items = response[0]
        
        var repositories = [Repository]()
        
        if let results = items.valueForKey("items") as? NSArray {
            for info in results {
                let repository = Repository(json: info as! NSDictionary)
                repositories.append(repository)
            }
        }
        
        return repositories
    }
    
    
}