

import Foundation

public enum HTTPStatusCode: Int {
    // Success
    case OK = 200
    case NoContent = 204
    
    // Client Errors
    case BadRequest = 400
    case Unauthorized = 401
    case Forbidden = 403
    case NotFound = 404
    case MethodNotAllowed = 405
    case AuthenticationTimeout = 419
    
    // Server Errors
    case InternalServerError = 500
    case GatewayTimeout = 504
    case NetworkTimeoutError = 599
    
    case requestTimeout = -1001
}

enum Result<T> {
    case Success(T)
    case Failure(Int!)
}

public extension HTTPStatusCode {
    public init?(HTTPResponse: NSHTTPURLResponse?) {
        if let value = HTTPResponse?.statusCode {
            self.init(rawValue: value)
        } else {
            return nil
        }
    }
}

public extension HTTPStatusCode {
    public func code() -> Int {
        return rawValue
    }
}