
import UIKit
import Haneke
import SwiftDate

class PullRequestsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var pullRequestTableView: UITableView!
    
    var userName: String!
    var repositoryName: String!
    var pullRequests: [PullRequest] = []
    var repositoryModel: Repository!
    
    override func viewWillAppear(animated: Bool) {
        self.pullRequestTableView.separatorColor = UIColor.clearColor()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.pullRequestTableView.delegate = self
        self.pullRequestTableView.dataSource = self
     
        if let valueName = repositoryModel.nameAuthor {
            self.userName = valueName
        }
        if let valueRepositoryName = repositoryModel.nameRepository {
            self.repositoryName = valueRepositoryName
        }
        
        self.title = self.repositoryName
    
        self.getLoadPullRequest()
    }
    
    // MARK: - Resquest Method
    func getLoadPullRequest() {
        
        let requestPullRequests = PullRequestsNetworkController()
        
        requestPullRequests.getPullRequests(self.userName, repositoryName: self.repositoryName) { (results) in
            if case .Success(let value) = results {
                print(value)
                self.pullRequests = value
            } else {
                print("[PullRequest] --> OCCURRED A PROBLEM, PLEASE TRY AGAIN LATER!")
            }
            
            self.pullRequestTableView.reloadData()
            self.pullRequestTableView.separatorColor = UIColor.grayColor()
        }
    
    }
    
    
    // MARK: - TableView Data Source
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.pullRequests.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("PullRequestCell", forIndexPath: indexPath) as! PullRequestDetailTableViewCell
        
        let pullRequest = pullRequests[indexPath.row]
        
        if let avatarUrl = pullRequest.avatarURL {
            let url = NSURL(string: avatarUrl)
            cell.ownerPullRequestImage.hnk_setImageFromURL(url!)
        }
        
        if let authorName = pullRequest.nameAuthor {
            cell.ownerPullRequestName.text = authorName
        }
        
        if let datePullRequest = pullRequest.date {
            let date = datePullRequest.toDateFromISO8601()
            cell.pullRequestDate.text = date!.toString(DateFormat.Custom(String().ambiguousDateFormatHour()))
        }
        
        if let pullRequestTitle = pullRequest.title {
            cell.pullRequestTitle.text = pullRequestTitle
        }
        
        if let pullRequestBody = pullRequest.body {
            cell.pullRequestBody.text = pullRequestBody
        }
        
        return cell
        
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let pullRequest = pullRequests[indexPath.row]
        if let authorName = pullRequest.url {
            if let url = NSURL(string: authorName) {
                UIApplication.sharedApplication().openURL(url)
            }
        }
    }
    

}
