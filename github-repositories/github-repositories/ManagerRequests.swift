

import UIKit
import Alamofire

class ManagerRequests: NSObject {
    
    var pageCount: Int = 0
    var listAllJSON = [NSDictionary]()
    
    func getRepositories(page page:Int, completion: (Result<[NSDictionary]>) -> Void) {
        UIApplication.sharedApplication().networkActivityIndicatorVisible = true
        
        Alamofire.request(.GET, Routes.repositoriesURL(page)).response {
            (request, response, data, error) in
            dispatch_async(dispatch_get_main_queue(), {
                guard error == nil else {
                    completion(.Failure(HTTPStatusCode(HTTPResponse: response)?.code()))
                    return
                }
                
                guard response!.statusCode == 200 else {
                    completion(.Failure(HTTPStatusCode(HTTPResponse: response)?.code()))
                    return
                }
                
                guard let jSONData = data,
                    let jSONObject = try? NSJSONSerialization.JSONObjectWithData(jSONData, options: []),
                    let jSONRepository = jSONObject as? NSDictionary
                    else {
                        completion(.Failure(HTTPStatusCode(HTTPResponse: response)?.code()))
                        return
                }
                
                self.validateJSON(json: jSONRepository)
                
                guard self.isLastPage(page) else {
                    completion(.Success(self.listAllJSON))
                    return
                }
                
                self.getRepositories(page: page+1, completion: { (result) in
                    completion(result)
                })
            })
        }
    }
    
    private func validateJSON(json JSONObject: NSDictionary) {
        //print(JSONObject)
        pageCount = (JSONObject.valueForKey("total_count")?.integerValue)!
        self.listAllJSON.append(JSONObject)
    }
    
    private func validateJSONPullRequest(json JSONObject: NSArray) {
        //print(JSONObject)
        let dictionary = JSONObject.objectAtIndex(0) as! NSDictionary
        self.listAllJSON.append(dictionary)
        
    }
    
    private func isLastPage(page: Int) -> Bool {
        return pageCount < 10 ? true : false
    }
    
    deinit {
        print("MEMORY DEALLOCATED - ManagerRequests")
    }
    
    func getPullRequests(userName:String, repositoryName:String, completion: (Result<[NSDictionary]>) -> Void) {
        UIApplication.sharedApplication().networkActivityIndicatorVisible = true
        
        Alamofire.request(.GET, Routes.pullRequestsURL(userName, repositoryName: repositoryName)).response {
            (request, response, data, error) in
    
                guard error == nil else {
                    completion(.Failure(HTTPStatusCode(HTTPResponse: response)?.code()))
                    return
                }
                
                guard response!.statusCode == 200 else {
                    completion(.Failure(HTTPStatusCode(HTTPResponse: response)?.code()))
                    return
                }
                
                guard let jSONData = data,
                    let jSONObject = try? NSJSONSerialization.JSONObjectWithData(jSONData, options: []),
                    let jSONPullRequests = jSONObject as? NSArray
                    else {
                        completion(.Failure(HTTPStatusCode(HTTPResponse: response)?.code()))
                        return
                }
                
                self.validateJSONPullRequest(json: jSONPullRequests)
                
                completion(.Success(self.listAllJSON))
                return
        }
    }

    
}