

import UIKit
import Alamofire

class RepositoriesNetworkController : NSObject {
    
    func getRepositories(page: Int, callback: (Result<[Repository]>) -> ()) {
        
        let ids = ManagerRequests()
        ids.getRepositories(page: page) { result in
            switch result {
            case .Success(let value):
                
                let validatorRepositories = JSONValidateRepositories()
                let repositories = validatorRepositories.convertToRepository(value)
                callback(.Success(repositories))
                
                break
                
            case .Failure(let error):
                callback(.Failure(error))
                break
            }
        }
        
    }
}
