

import UIKit

enum baseURL: String{
    case PROD = "https://api.github.com/"
}

enum complementURL: String {
    case repostories = "search/repositories?q=language:Java&sort=stars&page={:page}"
    case pullrequests = "repos/{:username}/{:repostoryname}/pulls?state=all"
}

class Routes {
    class func repositoriesURL(page:Int) -> String {
        let URL = baseURL.PROD.rawValue + complementURL.repostories.rawValue
            .stringByReplacingOccurrencesOfString("{:page}", withString:"\(page)")
        return URL
    }
    
    class func pullRequestsURL(userName:String, repositoryName:String) -> String {
        let URL = baseURL.PROD.rawValue + complementURL.pullrequests.rawValue
            .stringByReplacingOccurrencesOfString("{:username}", withString: userName)
            .stringByReplacingOccurrencesOfString("{:repostoryname}", withString: repositoryName)
        return URL
    }
}
