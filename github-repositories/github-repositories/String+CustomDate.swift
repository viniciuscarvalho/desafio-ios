
import UIKit

extension String {
    func ambiguousDateFormat() -> String {
        return "dd/MM/yyyy"
    }
    
    func ambiguousDateFormatHour() -> String {
        return "dd/MM/yyyy - HH:MM"
    }
}
