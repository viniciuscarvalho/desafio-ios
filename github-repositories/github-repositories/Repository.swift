

import UIKit

class Repository {
    
    let nameRepository: String?
    let descriptionRepository: String?
    let nameAuthor: String?
    let imageAuthor: String?
    let login: String?
    let numberStars: Int?
    let numberForks: Int?
    
    init(json: NSDictionary) {
        self.nameRepository = json["name"] as? String
        self.descriptionRepository = json["description"] as? String
        
        let owner = json["owner"] as! NSDictionary
        let login = owner.valueForKey("login")
        self.nameAuthor = login as? String
        
        let avatar_url = owner.valueForKey("avatar_url")
        self.imageAuthor = avatar_url as? String
        
        self.login = json["owner.login"] as? String
        self.numberStars = json["stargazers_count"] as? Int
        self.numberForks = json["forks"] as? Int
    }
}