

import UIKit

class PullRequestDetailTableViewCell: UITableViewCell {
    
    @IBOutlet weak var ownerPullRequestImage: UIImageView!
    @IBOutlet weak var ownerPullRequestName: UILabel!
    @IBOutlet weak var pullRequestDate: UILabel!
    @IBOutlet weak var pullRequestTitle: UILabel!
    @IBOutlet weak var pullRequestBody: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.ownerPullRequestImage.layer.cornerRadius = CGRectGetWidth(self.ownerPullRequestImage.frame) / 2
        self.ownerPullRequestImage.clipsToBounds = true;
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
