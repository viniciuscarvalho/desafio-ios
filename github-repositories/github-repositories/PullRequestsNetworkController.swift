

import UIKit
import Alamofire

class PullRequestsNetworkController : NSObject {

    func getPullRequests(userName: String, repositoryName: String, callback: (Result<[PullRequest]>) -> ()) {
        
        let ids = ManagerRequests()
        ids.getPullRequests(userName, repositoryName: repositoryName) { result in
            
            switch result {
            case .Success(let value):
                
                let validatePullRequests = JSONValidatePullRequests()
                let pullRequests = validatePullRequests.convertToPullRequests(value)
                callback(.Success(pullRequests))
                
                break
            
            case .Failure(let error):
                callback(.Failure(error))
                break
            }
        
        }
    
    }
    
}

