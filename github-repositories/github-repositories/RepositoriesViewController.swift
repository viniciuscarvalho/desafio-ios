

import UIKit
import Haneke

class RepositoriesViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var repositoryTableView: UITableView!
    
    var repositories = [Repository]()
    let requestRepositories = RepositoriesNetworkController()
    var page:Int!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "JavaPop"
        self.repositoryTableView.separatorColor = UIColor.clearColor()
        self.page = 1
        self.seachData(self.page)
        
    }
    
    // Get data and pagination
    private func seachData(page: Int) {
        
        self.requestRepositories.getRepositories(self.page) { (result) in
            
            if case .Success(let value) = result {
                self.repositories.appendContentsOf(value)
                self.repositoryTableView.separatorColor = UIColor.grayColor()
                self.repositoryTableView.reloadData()
            } else {
                print("OCURRED A PROBLEM, PLEASE TRY AGAIN LATER!")
            }
        }
        
    }
    
    // MARK: - TableView Data Source
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.repositories.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("RepositoriesCell", forIndexPath: indexPath) as! RepositoriesTableViewCell
        
        let repository = repositories[indexPath.row]
        
        if let imageAuthor = repository.imageAuthor {
            let url = NSURL(string: imageAuthor)
            cell.userImage.hnk_setImageFromURL(url!)
        }
        
        if let nameAuthor = repository.nameAuthor {
            cell.userNameLbl.text = nameAuthor
        }
        
        if let nameRepository = repository.nameRepository {
            cell.repositoryNameLbl.text = nameRepository
        }
        
        if let descriptionRepository = repository.descriptionRepository {
            cell.descriptionLbl.text = descriptionRepository
        }
        
        if let forksValue = repository.numberForks {
            let valueToString = String(forksValue)
            cell.forksLbl.text = valueToString
        }
        
        if let starValue = repository.numberStars {
            let valueToString = String(starValue)
            cell.starsLbl.text = valueToString
         }
        
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        var pullRequestView = PullRequestsViewController()
            pullRequestView = storyboard.instantiateViewControllerWithIdentifier("PullRequestView") as! PullRequestsViewController
            pullRequestView.repositoryModel = self.repositories[indexPath.row]
        self.navigationController!.pushViewController(pullRequestView, animated: true)
    }
}
